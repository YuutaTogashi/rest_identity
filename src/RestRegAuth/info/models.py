from django.db import models
from django.contrib.postgres.fields import JSONField, ArrayField
import datetime


class UserInfo(models.Model):
    first_name = models.CharField(max_length=100, default='')
    last_name = models.CharField(max_length=100, default='')
    city = models.CharField(max_length=100, default='')
    photo = models.TextField(default='')
    birth_date = models.DateField(default=datetime.date.today)
    sex = models.BooleanField(default=True)
    description = models.TextField(default='')
    owner = models.OneToOneField('auth.User', related_name='info', on_delete=models.CASCADE, primary_key=True)
    social = JSONField(default={})


class VerificationToken(models.Model):
    email = models.EmailField()
    token = models.CharField(default='', max_length=256, unique=True)
    created = models.DateTimeField()
    expire = models.DateTimeField()


class ResetToken(models.Model):
    email = models.EmailField()
    token = models.CharField(default='', max_length=256, unique=True)
    created = models.DateTimeField()
    expire = models.DateTimeField()
