from django.contrib import admin
from info.models import UserInfo, VerificationToken
from rest_framework_sso.models import SessionToken

admin.site.register(UserInfo)
admin.site.register(SessionToken)
admin.site.register(VerificationToken)
