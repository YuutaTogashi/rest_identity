from django.http import Http404
from django.contrib.auth.models import User
from django.conf import settings
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.contrib.auth import authenticate
from django.core.exceptions import ValidationError

from info.models import UserInfo, VerificationToken, ResetToken
from info.serializers import UserInfoSerializer, TokenSerializer, UserSerializer, ResetTokenSerializer
from info.permissions import IsOwner
from rest_framework_sso.models import SessionToken

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions

from datetime import datetime
import hashlib

class UserList(APIView):
    def get_object(self, pk):
        try:
            return UserInfo.objects.get(owner=User.objects.get(username=pk))
        except UserInfo.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        info = self.get_object(pk)
        serializer = UserInfoSerializer(info)
        return Response(serializer.data)


class UserDetail(APIView):
    permission_classes = (IsOwner, permissions.IsAuthenticated)
    login_required = True

    def get_object(self, pk):
        try:
            return UserInfo.objects.get(owner=pk)
        except UserInfo.DoesNotExist:
            raise Http404

    def get(self, request, format=None):
        info = self.get_object(request.user)
        serializer = UserInfoSerializer(info)
        return Response(serializer.data)

    def post(self, request, format=None):
        data = request.data
        data['owner'] = request.user

        serializer = UserInfoSerializer(data=data)

        if serializer.is_valid():
            serializer.save(owner=request.user)
            return  Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, format=None):
        info = self.get_object(request.user)
        serializer = UserInfoSerializer(info, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



class TokenView(APIView):
    def make_hash(self, id, email, created):
        m = hashlib.md5()
        m.update((str(id) + email + str(created)).encode('utf-8'))
        return m.hexdigest()

    def send(self, token, recipient):
        class_name = self.__class__.__name__.lower()
        subject = "Verification"
        url = settings.VERIFICATION_URL
        if 'reset' in class_name:
            subject = "Password reset"
            url = settings.PASSWORD_RESET_URL

        template_name = "{0}_message.html".format(class_name[0:len(class_name)-4])

        link = "{0}?t={1}".format(url, token)
        html = render_to_string(template_name, {'link': link})
        send_mail(subject, '', settings.EMAIL_HOST_USER, [recipient], fail_silently=False, html_message=html)

    def check_email(self, email):
        try:
            User.objects.get(email=email)
            return False
        except User.DoesNotExist:
            return True


class VerifyView(TokenView):
    def get(self, request, format=None):
        if settings.VERIFICATION_HEADER not in request.META:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        try:
            token = request.META[settings.VERIFICATION_HEADER]
            t = VerificationToken.objects.get(token=token)
            return Response({"email": t.email})
        except VerificationToken.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)


    def post(self, request, format=None):
        data = request.data
        data['created'] = datetime.now()
        data['expire'] = datetime.now() + settings.VERIFICATION_EXPIRE

        serializer = TokenSerializer(data=data)

        if serializer.is_valid():
            if not self.check_email(data['email']):
                return Response({'detail': 'Email already used.'}, status=status.HTTP_403_FORBIDDEN)
            VerificationToken.objects.filter(email=data['email']).delete()
            token = serializer.save()
            token.token = self.make_hash(token.id, token.email, token.created)
            token.save()
            self.send(token.token, token.email)
            return  Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ResetView(TokenView):
    def get_email(self, identity):
        try:
            user = User.objects.get(email=identity)
            return user.email
        except User.DoesNotExist:
            try:
                user = User.objects.get(username=identity)
                return user.email
            except User.DoesNotExist:
                return None

    def post(self, request, format=None):
        data = request.data
        data['created'] = datetime.now()
        data['expire'] = datetime.now() + settings.VERIFICATION_EXPIRE

        if 'identity' not in data:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        data['email'] = self.get_email(data['identity'])
        if not data['email']:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = ResetTokenSerializer(data=data)
        if serializer.is_valid():
            ResetToken.objects.filter(email=data['email']).delete()
            token = serializer.save()
            token.token = self.make_hash(token.id, token.email, token.created)
            token.save()
            self.send(token.token, token.email)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



class RegisterView(APIView):
    def get_email(self, request):
        if settings.VERIFICATION_HEADER not in request.META:
            return None
        try:
            token = request.META[settings.VERIFICATION_HEADER]
            t = VerificationToken.objects.get(token=token)
            return t.email
        except VerificationToken.DoesNotExist:
            return None

    def check_password(self, password, username):
        if len(password) < 8:
            return False
        if username in password:
            return False
        return True

    def post(self, request, format=None):
        email = self.get_email(request)
        if not email:
            return Response(status=status.HTTP_403_FORBIDDEN)

        data = request.data
        data['email'] = email
        serializer = UserSerializer(data=data)
        if serializer.is_valid():
            users = User.objects.filter(email=request.data['email'])
            if len(users) != 0:
                data = {'detail': 'User with this email already exists.'}
                return Response(data, status=status.HTTP_400_BAD_REQUEST)
            if not self.check_password(serializer.data['password'], serializer.data['username']):
                return Response({'detail': 'Bad password'}, status=status.HTTP_400_BAD_REQUEST)

            try:
                user = User.objects.create_user(username=serializer.data['username'],
                                         email=serializer.data['email'],
                                         password=serializer.data['password'])
                VerificationToken.objects.filter(email=user.email).delete()
                return Response(status=status.HTTP_201_CREATED)
            except Exception as ex:
                return Response({'detail': str(ex)}, status=status.HTTP_400_BAD_REQUEST)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ChangePassword(APIView):
    def clear_sessions(self, user):
        SessionToken.objects.filter(user=user).delete()

    def reset_password(self, request):
        try:
            token = request.META[settings.PASSWORD_RESET_HEADER]
            t = ResetToken.objects.get(token=token)
            try:
                user = User.objects.get(email=t.email)
                user.set_password(request.data['password'])
                user.save()

                self.clear_sessions(user)
                return Response(status=status.HTTP_200_OK)
            except User.DoesNotExist:
                Response(status=status.HTTP_404_NOT_FOUND)
            except ValidationError as ex:
                return Response({'detail': str(ex)}, status=status.HTTP_400_BAD_REQUEST)

        except ResetToken.DoesNotExist:
            return Response({'detail': 'Wrong token'}, status=status.HTTP_400_BAD_REQUEST)

    def change_password(self, user, password, new_password):
        try:
            current_user = authenticate(username=user.username, password=password)
            if current_user is None:
                return Response(status=status.HTTP_401_UNAUTHORIZED)

            user.set_password(new_password)
            user.save()

            self.clear_sessions(user)
            return Response(status=status.HTTP_200_OK)
        except ValidationError as ex:
            return Response({'detail': str(ex)}, status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, format=None):
        if settings.PASSWORD_RESET_HEADER in request.META:
            if 'password' not in request.data:
                return Response({"detail": "Password field is required"})
            return self.reset_password(request)

        if request.user:
            if 'password' not in request.data or 'new_password' not in request.data:
                return Response(status=status.HTTP_400_BAD_REQUEST)

            return self.change_password(request.user,
                                        request.data['password'],
                                        request.data['new_password'])

        return Response(status=status.HTTP_401_UNAUTHORIZED)


