from rest_framework import serializers
from info.models import UserInfo, VerificationToken, ResetToken
from django.contrib.auth.models import User
from django.conf import settings


class UserInfoSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    birth_date = serializers.DateField(required=False, input_formats=settings.DATE_INPUT_FORMATS)
    social = serializers.JSONField(default={})
    class Meta:
        model = UserInfo
        fields = ('first_name',
                  'last_name',
                  'city',
                  'birth_date',
                  'photo',
                  'owner',
                  'sex',
                  'description',
                  'social')

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username',
                  'email',
                  'password')


class TokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = VerificationToken
        fields = ('email',
                  'created',
                  'expire')

class ResetTokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = ResetToken
        fields = ('email',
                  'created',
                  'expire')