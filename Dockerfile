FROM python:3.5
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
ADD requirements.pip /code/
ADD ./src/RestRegAuth /code/
COPY ./entrypoint.sh /bin/entrypoint.sh

RUN pip install -r requirements.pip
RUN apt-get update

RUN chmod +x /bin/entrypoint.sh