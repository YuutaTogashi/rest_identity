#!/bin/bash

python manage.py collectstatic --noinput
python manage.py makemigrations
python manage.py migrate
gunicorn RestRegAuth.wsgi:application -w 2 -b :7001 --timeout 300
